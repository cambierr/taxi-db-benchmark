## Setup

1. Start CouchBase with `docker run -d --net host --name couchbase couchbase:5.5.1`
2. Setup your CouchBase cluster. I set 6Gb for data, 3Gb for indexes, 3Gb for analytics
3. Create a `trips` bucket with a lot of memory. I set 6Gb
4. Create a `trips` user with password `tripstrips`
5. Create a primary index on your bucket with `CREATE PRIMARY INDEX on trips USING GSI`

## Import

Just run the `Import.java` class and wait for completion.

## Test 1 (small dataset, on my laptop)

### Query service
```sql
SELECT cab_type, count(*)
FROM trips
GROUP BY cab_type
```
produces: `success | elapsed: 17.64s | execution: 17.64s | count: 1 | size: 36`

```sql
SELECT passenger_count, avg(total_amount)
FROM trips
GROUP BY passenger_count
```
produces: `success | elapsed: 18.67s | execution: 18.67s | count: 10 | size: 700`

```sql
SELECT passenger_count, count(*) trips
FROM trips
GROUP BY passenger_count, date_part_str(lpep_pickup_datetime, 'year')
```
produces:  `success | elapsed: 18.86s | execution: 18.86s | count: 15 | size: 1439`

### Analytics service

```sql
SELECT cab_type, count(*)
FROM trips
GROUP BY cab_type
```
produces: `success | elapsed: 1.87s | execution: 1.87s | count: 1 | size: 17 | processed objects: 793526`

```sql
SELECT passenger_count, avg(total_amount)
FROM trips
GROUP BY passenger_count
```
produces: ` success | elapsed: 2.87s | execution: 2.87s | count: 10 | size: 528 | processed objects: 793526`

```sql
SELECT passenger_count, count(*) trips
FROM trips
GROUP BY passenger_count, date_part_str(lpep_pickup_datetime, 'year')
```
produces: `success | elapsed: 4.05s | execution: 4.05s | count: 15 | size: 614 | processed objects: 793526`

## Test 2 (full dataset, on a cluster)

on the way!