package be.romaincambier.taxidb.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.UUID;

public class Import {


    public static void main(String[] args) throws Exception {

        CouchbaseCluster cluster = CouchbaseCluster.create("127.0.0.1");
        Bucket bucket = cluster.openBucket("trips", "tripstrips");

        for (File file : new File("./data/").listFiles()) {
            System.out.println("Processing file " + file.getName());

            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();

            if(line == null){
                throw new IllegalStateException("No title line");
            }

            String[] columns = line.split(",");

            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");

                if(values.length != columns.length){
                    continue;
                }

                JsonDocument document = JsonDocument.create(UUID.randomUUID().toString(), JsonObject.empty());

                for (int i = 0; i < values.length; i++) {
                    Object value = values[i];
                    try{
                        value = Double.parseDouble(value.toString());
                    } catch (NumberFormatException ex){
                        //ignore
                    }
                    document.content().put(columns[i], value);
                }

                bucket.upsert(document);
            }

            reader.close();
        }

    }
}
