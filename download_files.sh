#!/usr/bin/env bash

set -euo pipefail

mkdir -p ./data/

for (( year=2014; year<=$(date +"%Y"); year++ )); do
    for (( month=1; month<=12; month++ )); do
        textMonth=$month
        if [[ $textMonth -lt 10 ]]; then
            textMonth="0$textMonth"
        fi
        outputFile="green_tripdata_$year-$textMonth.csv"
        echo "Downloading file $outputFile..."
        status=$(curl --silent --write-out '%{http_code}\n' -o "./data/$outputFile" "https://s3.amazonaws.com/nyc-tlc/trip+data/$outputFile")
        if [[ $status == "404" ]]; then
            echo "It seems that file $outputFile isn't available yet; stopping!";
            exit 0;
        fi
    done
done